# ------------------------------------------------------------------------------
# Based on a work at https://github.com/docker/docker.
# ------------------------------------------------------------------------------
# Pull base image.
FROM actspace/ubuntu-supervisor
MAINTAINER actfire <actfire@gmail.com>

# ------------------------------------------------------------------------------
# apt-get update
RUN apt-get update

# ------------------------------------------------------------------------------
# Update Git Version
RUN apt-get install python-software-properties -y
RUN apt-get install software-properties-common -y
RUN add-apt-repository ppa:git-core/ppa -y
RUN apt-get install git -y

# ------------------------------------------------------------------------------
# Install base
#RUN apt-get install -y build-essential g++ curl apache2-utils git sshfs
#RUN apt-get install -y ant gcc g++ libkrb5-dev libmysqlclient-dev libssl-dev libsasl2-dev libsasl2-modules-gssapi-mit libsqlite3-dev libtidy-0.99-0 libxml2-dev libxslt-dev make libldap2-dev maven python-dev python-setuptools libgmp3-dev
RUN apt-get install -y build-essential g++ curl libssl-dev apache2-utils git libxml2-dev sshfs

# ------------------------------------------------------------------------------
# Install Node.js and version controller
#RUN curl --silent --location https://deb.nodesource.com/setup_0.12 | sudo bash -
RUN curl --silent --location https://deb.nodesource.com/setup_5.x | sudo bash -
RUN apt-get install -y nodejs
#RUN npm install -g n
#RUN n 5.*
#RUN npm install -g express-generator
#RUN npm i -g pm2

# ------------------------------------------------------------------------------
# Install Cloud9
RUN git clone https://github.com/c9/core.git /cloud9
WORKDIR /cloud9
RUN scripts/install-sdk.sh

# Tweak standlone.js conf
RUN sed -i -e 's_127.0.0.1_0.0.0.0_g' /cloud9/configs/standalone.js 

# Add supervisord conf
ADD conf/cloud9.conf /etc/supervisor/conf.d/

# ------------------------------------------------------------------------------
# Add volumes
RUN mkdir /workspace
VOLUME /workspace

# ------------------------------------------------------------------------------
# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ------------------------------------------------------------------------------
# Expose ports.
EXPOSE 80
EXPOSE 8080
EXPOSE 3000-3100

# ------------------------------------------------------------------------------
# Start supervisor, define default command.
CMD ["supervisord", "-c", "/etc/supervisor/supervisord.conf"]